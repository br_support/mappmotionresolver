#include <McAcpAx.h>
#include <Resolver.h>
#include "acp10par.h"
#include <bur/plctypes.h>

void Resolver(Resolver_typ *p) 
{
	switch (p->step)
	{
		case 0:
			
			if (p->Execute)
			{
				p->Busy = 1;
				p->step++;
			}
			break;

		case 1:
			
			p->i = 0;
			
			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TYPE;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Motortype;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_UINT;
				
			p->parIds[p->i].ParID = ACP10PAR_MOTOR_COMPATIBILITY;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Software_compatibility;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_UINT;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_WIND_CONNECT;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Winding_circuity;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_USINT;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_POLEPAIRS;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Number_of_polepairs;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_USINT;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_BRAKE_CURR_RATED;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Rated_current_brake;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_BRAKE_TORQ_RATED;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Rated_torque_brake;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_BRAKE_ON_TIME;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Delay_time_block_brake;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_BRAKE_OFF_TIME;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Delay_time_release_brake;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TEMPSENS_PAR1;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Temperature_sensor_par1;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TEMPSENS_PAR2;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Temperature_sensor_par2;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TEMPSENS_PAR3;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Temperature_sensor_par3;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TEMPSENS_PAR4;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Temperature_sensor_par4;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TEMPSENS_PAR5;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Temperature_sensor_par5;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TEMPSENS_PAR6;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Temperature_sensor_par6;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TEMPSENS_PAR7;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Temperature_sensor_par7;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TEMPSENS_PAR8;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Temperature_sensor_par8;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TEMPSENS_PAR9;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Temperature_sensor_par9;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TEMPSENS_PAR10;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Temperature_sensor_par10;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_VOLTAGE_RATED;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Rated_voltage;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_VOLTAGE_CONST;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Voltage_constant;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_SPEED_RATED;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Rated_operation_speed;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_SPEED_MAX;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Maximum_speed;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TORQ_STALL;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Stall_torque;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TORQ_RATED;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Nominal_torque;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TORQ_MAX;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Peak_torque;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TORQ_CONST;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Torque_constant;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_CURR_STALL;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Stall_current;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_CURR_RATED;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Nominal_current;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_CURR_MAX;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Peak_current;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_WIND_CROSS_SECT;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Line_cross_section_stator;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_STATOR_RESISTANCE;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Stator_winding_resistance;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_STATOR_INDUCTANCE;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Stator_winding_inductance;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_INERTIA;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Rotor_moment_of_inertia;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_COMMUT_OFFSET;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Rotoroffset_of_encoder;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_TAU_THERM;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Thermal_time_constant;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->parIds[p->i].ParID = ACP10PAR_MOTOR_WIND_TEMP_MAX;
			p->parIds[p->i].VariableAddress = (UDINT)&p->param.Maximum_stator_temperature;
			p->parIds[p->i++].DataType = mcACPAX_PARTYPE_REAL;

			p->step++;
			break;
			
		case 2:
			
			p->MC_BR_ProcessParID_AcpAx_0.Axis = p->Axis;
			p->MC_BR_ProcessParID_AcpAx_0.Execute = 1;
			p->MC_BR_ProcessParID_AcpAx_0.DataAddress = (UDINT)p->parIds;
			p->MC_BR_ProcessParID_AcpAx_0.NumberOfParIDs = p->i;
			p->MC_BR_ProcessParID_AcpAx_0.Mode = mcACPAX_PARID_SET;
			MC_BR_ProcessParID_AcpAx(&p->MC_BR_ProcessParID_AcpAx_0);
	
			if (p->MC_BR_ProcessParID_AcpAx_0.Done)
			{
				p->MC_BR_ProcessParID_AcpAx_0.Execute = 0;
				MC_BR_ProcessParID_AcpAx(&p->MC_BR_ProcessParID_AcpAx_0);
				p->step++;
			}
			
			if (p->MC_BR_ProcessParID_AcpAx_0.Error)
			{			
				p->ErrorID = p->MC_BR_ProcessParID_AcpAx_0.ErrorID;
				p->MC_BR_ProcessParID_AcpAx_0.Execute = 0;
				MC_BR_ProcessParID_AcpAx(&p->MC_BR_ProcessParID_AcpAx_0);
				p->step = 100;
			}
			break;
			
		case 3:
			
			p->Done = 1;
			p->Busy = 0;
			
			if (p->Execute == 0)
			{
				p->Done = 0;
				p->step = 0;
			}
			break;
		
		case 100:
			
			p->Error = 1;
			p->Busy = 0;

			if (p->Execute == 0)
			{
				p->Error = 0;
				p->ErrorID = 0;
				p->step = 0;
			}
			break;
		
	}
	
}
