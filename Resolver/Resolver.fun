
FUNCTION_BLOCK Resolver
	VAR_INPUT
		Axis : REFERENCE TO McAxisType;
		Execute : BOOL;
		param : MotorParameters;
	END_VAR
	VAR_OUTPUT
		Done : BOOL;
		Busy : BOOL;
		Error : BOOL;
		ErrorID : DINT;
	END_VAR
	VAR
		step : INT;
		MC_BR_ProcessParID_AcpAx_0 : MC_BR_ProcessParID_AcpAx;
		parIds : ARRAY[0..39] OF McAcpAxProcessParIDType;
		i : INT;
	END_VAR
END_FUNCTION_BLOCK
