
TYPE
	MotorParameters : 	STRUCT 
		Motortype : UINT;
		Software_compatibility : UINT;
		Winding_circuity : USINT;
		Rated_current_brake : REAL;
		Rated_torque_brake : REAL;
		Delay_time_block_brake : REAL;
		Delay_time_release_brake : REAL;
		Temperature_sensor_par1 : REAL;
		Temperature_sensor_par2 : REAL;
		Temperature_sensor_par3 : REAL;
		Temperature_sensor_par4 : REAL;
		Temperature_sensor_par5 : REAL;
		Temperature_sensor_par6 : REAL;
		Temperature_sensor_par7 : REAL;
		Temperature_sensor_par8 : REAL;
		Temperature_sensor_par9 : REAL;
		Temperature_sensor_par10 : REAL;
		Number_of_polepairs : USINT;
		Rated_voltage : REAL;
		Voltage_constant : REAL;
		Rated_operation_speed : REAL;
		Maximum_speed : REAL;
		Stall_torque : REAL;
		Nominal_torque : REAL;
		Peak_torque : REAL;
		Torque_constant : REAL;
		Stall_current : REAL;
		Nominal_current : REAL;
		Peak_current : REAL;
		Line_cross_section_stator : REAL;
		Stator_winding_resistance : REAL;
		Stator_winding_inductance : REAL;
		Rotor_moment_of_inertia : REAL;
		Rotoroffset_of_encoder : REAL;
		Maximum_stator_temperature : REAL;
		Thermal_time_constant : REAL;
	END_STRUCT;
END_TYPE
