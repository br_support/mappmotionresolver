

PROGRAM _INIT

	axis3par.Velocity := 10;
	axis3par.Acceleration := 10;
	axis3par.Deceleration := 10;
	
	axis3.MpLink := ADR(axis3ref);
	axis3.Parameters := ADR(axis3par);
	axis3.Enable := TRUE;
	
	Resolver_0.param.Motortype := 16#0002;
	Resolver_0.param.Software_compatibility := 16#0201;
	Resolver_0.param.Winding_circuity := 1;
	Resolver_0.param.Number_of_polepairs := 3;
	Resolver_0.param.Rated_current_brake := 0;
	Resolver_0.param.Rated_torque_brake := 0;
	Resolver_0.param.Delay_time_block_brake := 0;
	Resolver_0.param.Delay_time_release_brake := 0;
	Resolver_0.param.Temperature_sensor_par1 := 632;
	Resolver_0.param.Temperature_sensor_par2 := 2225;
	Resolver_0.param.Temperature_sensor_par3 := -30;
	Resolver_0.param.Temperature_sensor_par4 := 6;
	Resolver_0.param.Temperature_sensor_par5 := 36;
	Resolver_0.param.Temperature_sensor_par6 := 63;
	Resolver_0.param.Temperature_sensor_par7 := 87;
	Resolver_0.param.Temperature_sensor_par8 := 110;
	Resolver_0.param.Temperature_sensor_par9 := 130;
	Resolver_0.param.Temperature_sensor_par10 := 150;
	Resolver_0.param.Rated_voltage := 330;
	Resolver_0.param.Voltage_constant := 49;
	Resolver_0.param.Rated_operation_speed := 6000;
	Resolver_0.param.Maximum_speed := 12000;
	Resolver_0.param.Stall_torque := 2.6;
	Resolver_0.param.Nominal_torque := 1.2;
	Resolver_0.param.Peak_torque := 10.4;
	Resolver_0.param.Torque_constant := 0.81;
	Resolver_0.param.Stall_current := 3.21;
	Resolver_0.param.Nominal_current := 1.75;
	Resolver_0.param.Peak_current := 19.6;
	Resolver_0.param.Line_cross_section_stator := 0.46;
	Resolver_0.param.Stator_winding_resistance := 3.3;
	Resolver_0.param.Stator_winding_inductance := 0.015;
	Resolver_0.param.Rotor_moment_of_inertia := 0.00019;
	Resolver_0.param.Rotoroffset_of_encoder := 0;
	Resolver_0.param.Thermal_time_constant := 3600;
	Resolver_0.param.Maximum_stator_temperature := 110;

	
END_PROGRAM

PROGRAM _CYCLIC
	
	Resolver_0(Axis := ADR(axis3ref), Execute := TRUE);
	
	axis3();
	
	F_SWITCH := 5000;
	parIDs.ParID := 347; //ACP10PAR_F_SWITCH
	parIDs.VariableAddress := ADR(F_SWITCH);
	parIDs.DataType := mcACPAX_PARTYPE_REAL;
	
	MC_BR_ProcessParID_AcpAx_0(Axis := ADR(axis3ref), Execute := TRUE, DataAddress := ADR(parIDs), NumberOfParIDs := 1, Mode := mcACPAX_PARID_SET);
	 
END_PROGRAM



PROGRAM _EXIT
	
	axis3.Enable := FALSE;
	axis3();
	 
END_PROGRAM